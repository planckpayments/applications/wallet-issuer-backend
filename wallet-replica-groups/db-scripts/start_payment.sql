/*
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
*/


CREATE FUNCTION start_payment(payer UUID, payee UUID, sum INT, product UUID, metadata TEXT, OUT receipt_id UUID, OUT receipt_timestamp TIMESTAMP WITH TIME ZONE)
AS $$
DECLARE
  balance INT;
BEGIN
  SELECT wallet.balance FROM wallet
  WHERE id = payer
  INTO STRICT balance;

  IF balance < sum THEN
    RAISE EXCEPTION 'Insufficient Funds';
  END IF;

  UPDATE wallet
  SET balance = wallet.balance - sum
  WHERE wallet.id = payer;

  INSERT INTO receipts (id, wallet_id, recipient_id, value, timestamp, product_id, metadata)
  VALUES (
    uuid_generate_v4(),
    payer,
    payee,
    sum,
    CURRENT_TIMESTAMP,
    product,
    metadata
  )
  RETURNING id, timestamp INTO STRICT receipt_id, receipt_timestamp;

  INSERT INTO pending_payments VALUES (receipt_id);

EXCEPTION
  WHEN NO_DATA_FOUND THEN
    RAISE EXCEPTION 'Wallet not found';
  WHEN TOO_MANY_ROWS THEN
    RAISE EXCEPTION 'Wallet id not unique';
  WHEN UNIQUE_VIOLATION THEN
    RAISE EXCEPTION 'Generated receipt id already exists';

END $$ LANGUAGE plpgsql;
