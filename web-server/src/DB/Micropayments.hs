{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE OverloadedStrings, NoImplicitPrelude, MultiWayIf #-}

module DB.Micropayments (createWallet, refillWallet, getBalance, startPayment, collectPayments, getOutgoingSum) where

import Prelude
import qualified Data.ByteString.Char8 as BS_C8
import Data.UUID (UUID)
import Data.Int (Int32, Int64)
import Data.Time.Clock (UTCTime)
import Data.Text (Text)
import Data.ByteString (ByteString)
import Crypto.KeyManagement (PublicKey)
import Data.PlanckPayments.PaymentRequest (PaymentRequestRejection(RejectedInsufficientFunds))

import Hasql.Statement (Statement(Statement))
import qualified Hasql.Decoders as Decoders
import qualified Hasql.Encoders as Encoders
import qualified Hasql.Session as Session
import qualified Hasql.Transaction as Transaction
import Hasql.Transaction.Sessions (transaction, Mode(Read,Write), IsolationLevel(ReadCommitted, Serializable))
import Hasql.Session (QueryError(QueryError), CommandError(ResultError), ResultError(ServerError))
import DB.Internals.Base (withConnection)
import qualified DB.Internals.Params as Params
import qualified DB.Internals.Result as Result

import Contravariant.Extras.Contrazip (contrazip3, contrazip5)
import Control.Monad (void)
import System.Environment ( getEnv )
import Control.Exception ( catch, throwIO )

getReplicaSets :: IO [ByteString]
getReplicaSets =
  BS_C8.split ';' . BS_C8.pack <$> getEnv "WALLET_REPLICA_GROUPS"


withConnectionToWallet :: UUID -> Session.Session a -> IO a
withConnectionToWallet walletId session = do
  [walletReplicaGroup1, walletReplicaGroup2] <- getReplicaSets
  -- Select the appropriate replica group
  --NOTE: this is hacky. We'd like a hard-to-guess system e.g. based on hashmaps and/or walletIds encrypted with a symmetric key
  let selectedReplicaGroup = if | walletId < (read "88888888-8888-8888-8888-888888888888")  -> walletReplicaGroup1
                                | otherwise                                                 -> walletReplicaGroup2
  withConnection selectedReplicaGroup session

--TODO: should probably succeed in all DBs or be rolled back, unsure
withConnectionToAllWallets :: Session.Session a -> IO [a]
withConnectionToAllWallets session = do
  replicaSets <- getReplicaSets
  mapM (\db -> withConnection db session) replicaSets

balanceStatement :: Statement UUID Int32
balanceStatement =
  Statement
    "SELECT get_balance($1)"
    Params.uuid
    Result.int32
    True

--NOTE.: this should ideally not touch the live balance DB but collect information from a stale source
getBalance :: UUID -> IO Int32
getBalance walletId =
  let
    t = Transaction.statement walletId balanceStatement
    session = transaction ReadCommitted Read t
  in
    withConnectionToWallet walletId session


createWalletStatement :: Statement (UUID, Int32, UUID) ()
createWalletStatement =
  Statement
    "SELECT create_wallet($1, $2, $3)"
    (contrazip3 Params.uuid Params.int32 Params.uuid)
    Decoders.noResult
    True

createWallet :: UUID -> Int32 -> UUID -> IO ()
createWallet walletId value prepaidCode =
  let
    t1 = Transaction.sql "SET LOCAL synchronous_commit = ON"
    t2 = Transaction.statement (walletId, value, prepaidCode) createWalletStatement
    session = transaction Serializable Write (t1 >> t2)
  in
    withConnectionToWallet walletId session



refillWalletStatement :: Statement (UUID, Int32, UUID) ()
refillWalletStatement =
  Statement
    "SELECT fill_wallet($1, $2, $3)"
    (contrazip3 Params.uuid Params.int32 Params.uuid)
    Decoders.noResult
    True

refillWallet :: UUID -> Int32 -> UUID -> IO ()
refillWallet walletId value prepaidCode =
  let
    t1 = Transaction.sql "SET LOCAL synchronous_commit = ON"
    t2 = Transaction.statement (walletId, value, prepaidCode) refillWalletStatement
    session = transaction Serializable Write (t1 >> t2)
  in
    withConnectionToWallet walletId session


startPaymentStatement :: Statement (UUID, UUID, Int32, UUID, Maybe Text) UTCTime
startPaymentStatement =
  Statement
    "SELECT start_payment($1, $2, $3, $4, $5)"
    (contrazip5 Params.uuid Params.uuid Params.int32 Params.uuid Params.optionalText)
    Result.timestamptz
    True

startPayment :: UUID -> UUID -> Int32 -> UUID -> Maybe Text -> IO UTCTime
startPayment walletId recipientId value productId metadata =
  let
    t1 = Transaction.sql "SET LOCAL synchronous_commit = OFF"
    t2 = Transaction.statement (walletId, recipientId, value, productId, metadata) startPaymentStatement
    session = transaction Serializable Write (t1 >> t2)
  in
    catch
      ( withConnectionToWallet walletId session )
      ( \err -> case err of
        QueryError _ _ (ResultError (ServerError "P0001" "Insufficient Funds" _ _)) ->
          throwIO RejectedInsufficientFunds
        _ -> throwIO err
      )


-- NOTE: placeholders, not scrutinized yet
collectPaymentsStatement :: Statement () ()
collectPaymentsStatement =
  Statement
    "SELECT update_payments()"
    Encoders.noParams
    Decoders.noResult
    True

collectPayments :: IO ()
collectPayments =
  let
    t1 = Transaction.sql "SET LOCAL synchronous_commit = ON"
    t2 = Transaction.statement () collectPaymentsStatement
    session = transaction Serializable Write (t1 >> t2)
  in
    void $ withConnectionToAllWallets session


getOutgoingSumStatement :: Statement UUID Int64
getOutgoingSumStatement =
  Statement
    "SELECT get_outgoing_sum($1)"
    Params.uuid
    Result.int64
    True

getOutgoingSum :: UUID -> IO Int64
getOutgoingSum recipient =
  let
    t1 = Transaction.sql "SET LOCAL synchronous_commit = ON"
    t2 = Transaction.statement recipient getOutgoingSumStatement
    session = transaction Serializable Read (t1 >> t2)
  in
  sum <$> withConnectionToAllWallets session
