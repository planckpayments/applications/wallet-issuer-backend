{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE OverloadedStrings, ScopedTypeVariables, NoImplicitPrelude, DerivingStrategies #-}

module Server.Request (readLBS, readJSON, readJWS, readJWS', BadArgumentsException(BadArgumentsException)) where

import Prelude (Show, IO, Either(Left, Right), return, ($))
import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS
import qualified Data.ByteString.Char8
import qualified Data.Aeson
import qualified Network.Wai

import Crypto.KeyManagement (PublicKey)
import Data.ByteString ( ByteString )
import Network.Wai ( Request )
import Data.Aeson ( FromJSON )
import Data.JWT (FromJWT, verify, verify')

import Control.Exception (Exception, throw)

data BadArgumentsException = BadArgumentsException ByteString
  deriving stock Show

instance Exception BadArgumentsException
  -- displayException can be added for human-friendly error messages

readLBS :: Request -> IO LBS.ByteString
readLBS = Network.Wai.strictRequestBody


readJSON :: FromJSON a => Request -> IO a
readJSON req = do
  requestBody <- Network.Wai.strictRequestBody req
  case Data.Aeson.eitherDecode requestBody of
    Left err -> throw $ BadArgumentsException $ Data.ByteString.Char8.pack err
    Right args -> return args


readJWS :: FromJWT a => Request -> IO a
readJWS req = do
  requestBody <- Network.Wai.strictRequestBody req
  claimsResult <- verify $ LBS.toStrict requestBody
  return claimsResult


readJWS' :: FromJWT a => (BS.ByteString -> IO PublicKey) -> Request -> IO a
readJWS' keyGetter req = do
  requestBodyLazy <- Network.Wai.strictRequestBody req
  let requestBody = LBS.toStrict requestBodyLazy
  publicKey <- keyGetter requestBody
  claimsResult <- verify' publicKey requestBody
  return claimsResult
