{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE OverloadedStrings, MultiWayIf, NoImplicitPrelude #-}

module Server.Endpoint ((~>), endpoint, router) where

import Prelude

import qualified Data.Map
import qualified Network.Wai
import qualified Data.Maybe
import qualified Data.Text

import qualified Server.Response

import Server.Response ( Response )
import Network.HTTP.Types ( Method )
import Network.Wai (Application, Request)
import Data.Map ((!?))
import Data.Text (Text)

type Path = Text

(~>) :: Path -> Application -> (Path, Application)
(~>) = (,)

endpoint :: Method -> (Request -> IO Response) -> Application
endpoint method handleRequest request respond =
  let
    createResponse :: IO Response
    createResponse = case Network.Wai.requestMethod request of
      "OPTIONS"       -> return $ Server.Response.emptySuccessResponse
      m | m == method -> handleRequest request
        | otherwise   -> return $ Server.Response.badMethod method
  in do
    response <- createResponse
    respond $ Server.Response.addCorsHeaders method response



router :: [ ( Path, Application ) ] -> Application
router subhandlers =
  let
    routeMap = Data.Map.fromList subhandlers

    app :: Application
    app request respond =
      let
        requestedPath :: Maybe Text
        requestedPath = case Network.Wai.pathInfo request of
          _head : _tail -> Just _head
          [] -> Nothing

        handler :: Maybe Application
        handler =
          requestedPath >>= (!?) routeMap

        editedRequest :: Network.Wai.Request
        editedRequest = request { Network.Wai.pathInfo = tail $ Network.Wai.pathInfo request }
      in
        case handler of
          Just handler' -> handler' editedRequest respond
          Nothing -> do
            putStrLn $ "Could not find " ++ Data.Text.unpack (Data.Maybe.fromJust requestedPath)
            respond $ Server.Response.notFound
    in app
