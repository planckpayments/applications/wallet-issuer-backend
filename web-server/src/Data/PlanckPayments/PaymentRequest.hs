{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE NoImplicitPrelude, OverloadedStrings, TemplateHaskell, DerivingStrategies #-}

module Data.PlanckPayments.PaymentRequest (PaymentRequest(PaymentRequest), walletId, recipientId, value, productId, metadata, PaymentRequestRejection(RejectedInsufficientFunds)) where

import Prelude (Show)

import Data.UUID (UUID)
import Data.Int (Int32)
import Data.Text (Text)
import Data.Maybe (Maybe)
import Data.Semigroup ((<>))
import Data.Aeson ((.=))
import Data.Aeson.TH (deriveJSON, defaultOptions)

import Data.JWT (FromJWT, ToJWT, toClaims)
import Control.Exception (Exception)

data PaymentRequest = PaymentRequest
  { walletId :: !UUID
  , recipientId :: !UUID
  , value :: !Int32
  , productId :: !UUID
  , metadata :: !(Maybe Text)
  }

$(deriveJSON defaultOptions 'PaymentRequest)

instance FromJWT PaymentRequest
instance ToJWT PaymentRequest where
  toClaims req =
    ( "walletId"    .= walletId req
    <>"recipientId" .= recipientId req
    <>"value"       .= value req
    <>"productId"   .= productId req
    <>"metadata"    .= metadata req
    )

data PaymentRequestRejection
  = RejectedInsufficientFunds
  deriving stock (Show)

instance Exception PaymentRequestRejection
