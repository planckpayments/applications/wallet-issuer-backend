{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE NoImplicitPrelude, OverloadedStrings, DeriveGeneric #-}

module Test.Endpoint.Balance (run) where

import Prelude
import Network.HTTP.Req
import Test.HUnit (Test(TestCase), assertEqual)
import Data.PlanckPayments.BalanceRequest
import Data.UUID (UUID)
import Data.Int (Int32)
import Crypto.KeyManagement (PrivateKey)
import Data.JWT (sign')
import Data.ByteString.Char8 (unpack)

run :: UUID -> PrivateKey -> IO Int32
run walletId privateKey = do
  signedJWT <- sign' privateKey $ BalanceRequest walletId
  r <- runReq defaultHttpConfig $ do
    res <- req POST (http "localhost" /: "api" /: "balance") (ReqBodyBs signedJWT) bsResponse (port 8080)
    return (responseBody res)
  return $ read $ unpack r
