{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE NoImplicitPrelude, OverloadedStrings #-}

module Test.Endpoint.Ping (test) where

import Prelude
import Network.HTTP.Req
import Test.HUnit (Test(TestCase, TestLabel), assertEqual)

test :: Test
test = TestLabel "Server liveliness" $ TestCase $ do
  r <- runReq defaultHttpConfig $ do
    res <- req GET (http "localhost" /: "api" /: "ping") NoReqBody bsResponse (port 8080)
    return $ responseBody res
  assertEqual "Server ponged" r "Pong"
