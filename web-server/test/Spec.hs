{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE NoImplicitPrelude, OverloadedStrings, DeriveGeneric #-}

module Main (main) where

import Prelude
import Test.HUnit (runTestTT, Test(TestCase, TestLabel, TestList), assertEqual, assertFailure)
import Network.HTTP.Req (HttpException(VanillaHttpException))
import Network.HTTP.Client (HttpException(HttpExceptionRequest), HttpExceptionContent(StatusCodeException), responseStatus)
import Network.HTTP.Types (statusCode)
import Control.Exception (throwIO, catch)

import Test.JWT
import qualified Test.Endpoint.CreateWallet
import qualified Test.Endpoint.RefillWallet
import qualified Test.Endpoint.Balance
import qualified Test.Endpoint.Pay
import qualified Test.Endpoint.Ping
import qualified Test.Endpoint.PublicKey
import qualified TestingKeys
import qualified Data.UUID.V4

main :: IO ()
main = do
  results <- runTestTT $ TestList
    [ Test.JWT.tests
    , Test.Endpoint.Ping.test
    , Test.Endpoint.PublicKey.test
    , testPaymentProcess
    , testLowBalanceFailure
    ]
  print results

testPaymentProcess :: Test
testPaymentProcess = TestLabel "Correct payment process" $ TestCase $ do
  privateKey <- either fail return $ TestingKeys.loadPrivateKey1
  publicKey <- either fail return $ TestingKeys.loadPublicKey1
  walletId <- Data.UUID.V4.nextRandom
  prepaidCode1 <- Data.UUID.V4.nextRandom
  prepaidCode2 <- Data.UUID.V4.nextRandom
  recipient <- Data.UUID.V4.nextRandom
  productId <- Data.UUID.V4.nextRandom
  let deposited1 = 5000
  let transferSum = 11
  let deposited2 = 1000
  Test.Endpoint.CreateWallet.run walletId deposited1 publicKey prepaidCode1
  balance1 <- Test.Endpoint.Balance.run walletId privateKey
  assertEqual "Balance returned differs from what was deposited" balance1 deposited1

  Test.Endpoint.Pay.run walletId recipient transferSum productId Nothing privateKey
  balance2 <- Test.Endpoint.Balance.run walletId privateKey
  assertEqual "Balance2 returned differs from deposited - spent" balance2 (deposited1-transferSum)

  Test.Endpoint.RefillWallet.run walletId deposited2 prepaidCode2
  balance3 <- Test.Endpoint.Balance.run walletId privateKey
  assertEqual "Balance3 returned differs from deposited1 - spent + deposited2" balance3 (deposited1-transferSum+deposited2)



testLowBalanceFailure :: Test
testLowBalanceFailure = TestLabel "Currect rejection of payment request when balance is low" $ TestCase $ do
  privateKey <- either fail return $ TestingKeys.loadPrivateKey1
  publicKey <- either fail return $ TestingKeys.loadPublicKey1
  walletId <- Data.UUID.V4.nextRandom
  prepaidCode <- Data.UUID.V4.nextRandom
  recipient <- Data.UUID.V4.nextRandom
  productId <- Data.UUID.V4.nextRandom

  Test.Endpoint.CreateWallet.run walletId 1000 publicKey prepaidCode
  catch
    ( do
      Test.Endpoint.Pay.run walletId recipient 1020 productId Nothing privateKey
      assertFailure "Payments with insufficient balance should be rejected"
    )
    ( \e -> case e of
      VanillaHttpException (HttpExceptionRequest _ (StatusCodeException response _)) -> assertEqual "Payment rejection due to low balance status code" (statusCode $ responseStatus response) 403
      _ -> throwIO e
    )
