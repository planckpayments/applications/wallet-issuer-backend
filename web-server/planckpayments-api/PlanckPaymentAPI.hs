{-
Copyright (C) 2018-2022, Oscar Leijendekker.

This file is part of the Planck Payments wallet issuer backend.

The Planck Payments wallet issuer backend is free software: you can
redistribute it and/or modify it under the terms of the GNU Affero General
Public License as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

The Planck Payments wallet issuer backend is distributed in the hope that it
will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General
Public License for more details.

You should have received a copy of the GNU Affero General Public License along
with The Planck Payments wallet issuer backend. If not, see 
<https://www.gnu.org/licenses/>.
-}

{-# LANGUAGE OverloadedStrings, NoImplicitPrelude #-}

module Main
    ( main
    , startServer
    , getPort
    ) where

import Prelude

import Data.PlanckPayments.BalanceRequest (BalanceRequest(BalanceRequest))
import Data.PlanckPayments.PaymentRequest (PaymentRequest(PaymentRequest), PaymentRequestRejection(RejectedInsufficientFunds))
import Data.PlanckPayments.Receipt (Receipt(Receipt))
import Data.PlanckPayments.WalletCreationRequest (WalletCreationRequest(WalletCreationRequest))
import Data.PlanckPayments.WalletRefillRequest (WalletRefillRequest(WalletRefillRequest))

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as LBS

import Server.Endpoint ( endpoint, router, (~>))
import Server.Response (successResponseJSON, successResponseLBS, serverError, paymentRequired)
import Server.Request (readJWS, readJWS')
import Data.JWT (sign, peekClaim)
import Crypto.KeyManagement (PublicKey, getPublicKeyPEM)
import qualified DB.Micropayments
import qualified DB.PublicKeys

import qualified Network.Wai as Wai
import Network.Wai (Response, Application)
import qualified Network.Wai.Handler.Warp as Warp

import System.Environment ( getEnv )
import System.IO ( hFlush, stdout, stderr )
import Control.Exception ( SomeException, catch )
import Control.Monad ((>=>))

main :: IO ()
main = startServer

getPort :: IO Int
getPort = do
  portStr <- getEnv "PORT"
  return $ read portStr

flushIOBuffers :: IO ()
flushIOBuffers = do
  hFlush stdout
  hFlush stderr

startServer :: IO ()
startServer =
  let
    onException :: (Maybe Wai.Request -> SomeException -> IO ())
    onException r e = do
      Warp.defaultOnException r e
      flushIOBuffers

    settings :: Int -> Warp.Settings
    settings port =
      Warp.setPort port
      $ Warp.setOnException onException
      $ Warp.setOnExceptionResponse serverError
      $ Warp.defaultSettings
  in
    do
      port <- getPort
      Warp.runSettings (settings port) app


app :: Application
app request respond =
  let
    server =
      router
        [ "api" ~> router
          [ "ping"                  ~> endpoint "GET"  (\_ -> pingEndpoint)
          , "public-key"            ~> endpoint "GET"  (\_ -> publicKeyEndpoint)
          , "create-wallet"         ~> endpoint "POST" (readJWS >=> createWalletEndpoint)
          , "refill-wallet"         ~> endpoint "POST" (readJWS >=> refillWalletEndpoint)
          , "balance"               ~> endpoint "POST" (readJWS' fetchPublicKeyForWalletIdClaim >=> balanceEndpoint)
          , "pay"                   ~> endpoint "POST" (readJWS' fetchPublicKeyForWalletIdClaim >=> payEndpoint)
          --, "update-payment-status" ~> endpoint "POST" (\_ -> updatePaymentStatus) -- TODO: should be removed and replaced by internal system to prevent DoS attacks
          ]
        ]
  in
    do
      responseReceived <- server request respond
      flushIOBuffers
      return responseReceived

fetchPublicKeyForWalletIdClaim :: BS.ByteString -> IO PublicKey
fetchPublicKeyForWalletIdClaim jwtBS = do
  walletId <- peekClaim "walletId" jwtBS
  DB.PublicKeys.retrievePublicKey walletId

pingEndpoint :: IO Response
pingEndpoint =
  return $ successResponseLBS "Pong"

publicKeyEndpoint :: IO Response
publicKeyEndpoint = do
  key <- getPublicKeyPEM
  return $ successResponseLBS key

balanceEndpoint :: BalanceRequest -> IO Response
balanceEndpoint (BalanceRequest walletId) = do
    balance <- DB.Micropayments.getBalance walletId
    return $ successResponseJSON balance

payEndpoint :: PaymentRequest -> IO Response
payEndpoint (PaymentRequest walletId recipientId value productId metadata) =
  catch
    ( do
      timestamp <- DB.Micropayments.startPayment walletId recipientId value productId metadata
      signedJwt <- sign $ Receipt recipientId value productId metadata timestamp
      return $ successResponseLBS $ LBS.fromStrict signedJwt
    )
    ( \e ->
      case e of
        RejectedInsufficientFunds ->
          return $ paymentRequired "balance too low"
    )

createWalletEndpoint :: WalletCreationRequest -> IO Response
createWalletEndpoint (WalletCreationRequest walletId value publicKey prepaidCode) =
  do
    DB.PublicKeys.storePublicKey walletId publicKey
    DB.Micropayments.createWallet walletId value prepaidCode
    return $ successResponseLBS "Ok"

refillWalletEndpoint :: WalletRefillRequest -> IO Response
refillWalletEndpoint (WalletRefillRequest walletId value prepaidCode) =
  do
    DB.Micropayments.refillWallet walletId value prepaidCode
    return $ successResponseLBS "Ok"
{-
updatePaymentStatus :: IO Response
updatePaymentStatus = do
  _ <- forkIO Micropayments.collectPayments
  return $ successResponseLBS "Ok"
-}
